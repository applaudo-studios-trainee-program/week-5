# Trainee Program - Week 5

## Installation

There is **no necessary configuration**, you just need to use one of the link(s) below to see the last deployed version of the project

## Deployment

You can see the last deployed version of the project by going to any of these links

[Vercel](https://week-5.b-mendoza.vercel.app/)
