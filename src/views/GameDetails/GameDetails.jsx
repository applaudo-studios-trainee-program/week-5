import { useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import Comment from 'components/Comment/Comment';
import Spinner from 'assets/Icons/Spinner/Spinner';
import MetaInfo from 'components/MetaInfo/MetaInfo';
import { getGameById } from 'helpers/gamesHelpers';
import Styles from './GameDetails.module.css';

/**
 * @typedef   {import('helpers/gamesHelpers').GameData} GameData
 */

/**
 * @typedef   {Object.<string, string>} Params
 */

/**
 * @typedef     {Object} Props
 * @property    {Params} params
 */

/**
 * @param       {Props}
 */

function GameDetails({ params }) {
  const { id } = params;

  /**
   * @type {[GameData, React.Dispatch<React.SetStateAction<GameData>>]}
   */

  const [gameData, setGameData] = useState({});

  useEffect(() => getGameById({ gameId: id }).then(setGameData), []);

  if (Object.keys(gameData).length === 0) return <Spinner />;

  const { name, gameArt, metaInfo, comments } = gameData;

  const { url } = gameArt;

  return (
    <div className={Styles.container}>
      <h1 className={Styles.title}>
        <small>{name}</small>
      </h1>

      <img
        className={Styles.image}
        src={url}
        alt={`${name} Cover`}
        title={`${name} Cover`}
      />

      <section className={Styles['meta-container']}>
        {metaInfo.map((metaData) => (
          <MetaInfo key={metaData.id} {...metaData} />
        ))}
      </section>

      <section className={Styles['comment-list']}>
        <h2 className={Styles['comment-list-title']}>Comments</h2>

        {comments.length ? (
          comments.map((comment) => <Comment key={comment.id} {...comment} />)
        ) : (
          <p style={{ margin: 0 }}>There are no comments yet 😮</p>
        )}
      </section>
    </div>
  );
}

GameDetails.propTypes = {
  params: PropTypes.shape({
    id: PropTypes.string.isRequired,
  }).isRequired,
};

export default GameDetails;
