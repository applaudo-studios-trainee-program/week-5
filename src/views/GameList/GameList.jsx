import { Suspense, useCallback, useMemo, lazy } from 'react';
import PropTypes from 'prop-types';

import Spinner from 'assets/Icons/Spinner/Spinner';
import Button from 'components/Button/Button';
import { usePagination } from 'hooks/usePagination';
import { usePrevious } from 'hooks/usePrevious';
import Styles from './GameList.module.css';

const LazyCard = lazy(() => import('components/Card/Card'));

/**
 * @typedef     {Object} Props
 * @property    {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param       {Props}
 */

function GameList({ setPathname }) {
  const { itemsToShow, actualPage, totalPages, setActualPage } = usePagination({
    itemsPerPage: 8,
  });

  const actualPageRef = usePrevious({ value: actualPage });

  /**
   * @param   {React.MouseEvent<HTMLAnchorElement>} event
   */

  const handleAnchorClick = (event) => {
    event.preventDefault();

    /**
     * @type  {HTMLAnchorElement}
     */

    const anchor = event.currentTarget;

    setPathname(anchor.pathname);
  };

  const handleBackClick = useCallback(() => setActualPage(actualPageRef - 1));

  const handleNextClick = useCallback(() => setActualPage(actualPageRef + 1));

  const List = useMemo(
    () =>
      itemsToShow.map((gameData) => (
        <Suspense key={gameData.id} fallback={<Spinner />}>
          <a href={`game-details/${gameData.id}`} onClick={handleAnchorClick}>
            <LazyCard setPathname={setPathname} {...gameData} />
          </a>
        </Suspense>
      )),
    [itemsToShow]
  );

  return (
    <div className={Styles.container}>
      <section className={Styles['game-list']}>{List}</section>

      <div className={Styles.actions}>
        <Button
          content="Back"
          disabled={actualPage === 0}
          onClickFn={handleBackClick}
        />

        <Button
          content="Next"
          disabled={actualPage === totalPages}
          onClickFn={handleNextClick}
        />
      </div>
    </div>
  );
}

GameList.propTypes = {
  setPathname: PropTypes.func.isRequired,
};

export default GameList;
