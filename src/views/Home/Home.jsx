import { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import Hero from 'components/UI/Hero/Hero';
import Card from 'components/Card/Card';
import { getGames } from 'helpers/gamesHelpers';
import Styles from './Home.module.css';

/**
 * @typedef     {Object} Props
 * @property    {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param       {Props}
 */

function Home({ setPathname }) {
  const isMountedRef = useRef(false);

  const [recentlyUpdated, setRecentlyUpdated] = useState([]);
  const [newReleases, setNewReleases] = useState([]);

  useEffect(() => {
    isMountedRef.current = true;

    return () => {
      isMountedRef.current = false;
    };
  }, []);

  useEffect(() => {
    getGames({ offset: 4, sortBy: 'created_at' }).then((response) => {
      if (isMountedRef.current) setNewReleases(response);
    });

    getGames({ offset: 4, sortBy: 'updated_at' }).then((response) => {
      if (isMountedRef.current) setRecentlyUpdated(response);
    });
  }, []);

  /**
   * @param   {React.MouseEvent<HTMLAnchorElement>} event
   */

  const handleAnchorClick = (event) => {
    event.preventDefault();

    /**
     * @type  {HTMLAnchorElement}
     */

    const anchor = event.currentTarget;

    setPathname(anchor.pathname);
  };

  return (
    <>
      <Hero setPathname={setPathname} />

      <section className={Styles['game-section']}>
        <h2 className={Styles['section-title']}>Recently Added</h2>

        {newReleases.map((gameData) => (
          <a
            href={`game-details/${gameData.id}`}
            key={gameData.id}
            onClick={handleAnchorClick}
          >
            <Card setPathname={setPathname} {...gameData} />
          </a>
        ))}
      </section>

      <section className={Styles['game-section']}>
        <h2 className={Styles['section-title']}>Recently Updated</h2>

        {recentlyUpdated.map((gameData) => (
          <a
            href={`game-details/${gameData.id}`}
            key={gameData.id}
            onClick={handleAnchorClick}
          >
            <Card setPathname={setPathname} {...gameData} />
          </a>
        ))}
      </section>
    </>
  );
}

Home.propTypes = {
  setPathname: PropTypes.func.isRequired,
};

export default Home;
