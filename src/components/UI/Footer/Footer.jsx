import * as React from 'react';
import PropTypes from 'prop-types';

import Social from 'components/Social/Social';
import Facebook from 'assets/Icons/Facebook';
import Instagram from 'assets/Icons/Instagram';
import Styles from './Footer.module.css';

/**
 * @typedef     {Object} Props
 * @property    {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param       {Props}
 */

function Footer({ setPathname }) {
  /**
   * @param   {React.MouseEvent<HTMLAnchorElement>} event
   */

  const handleAnchorClick = (event) => {
    event.preventDefault();

    /**
     * @type  {HTMLAnchorElement}
     */

    const anchor = event.target;

    setPathname(anchor.pathname);
  };

  return (
    <footer className={Styles.container}>
      <h3 className={Styles.title}>React Stack</h3>

      <section className={Styles.section}>
        <h4 className={Styles['section-title']}>Social</h4>

        <Social socialIcon={Facebook} username="@react-stack" />
        <Social socialIcon={Instagram} username="@react_stack" />
      </section>

      <section className={Styles.section}>
        <h4 className={Styles['section-title']}>Menu</h4>

        <nav className={Styles.menu}>
          <ul className={Styles['menu-list']}>
            <li className={Styles['menu-item']}>
              <a
                className={Styles['menu-link']}
                href="/"
                onClick={handleAnchorClick}
              >
                Home
              </a>
            </li>
            <li className={Styles['menu-item']}>
              <a
                className={Styles['menu-link']}
                href="/game-list"
                onClick={handleAnchorClick}
              >
                Game List
              </a>
            </li>
          </ul>
        </nav>
      </section>
    </footer>
  );
}

Footer.propTypes = {
  setPathname: PropTypes.func.isRequired,
};

export default React.memo(Footer);
