import * as React from 'react';
import PropTypes from 'prop-types';

import Menu from 'assets/Icons/Menu';
import Styles from './Navbar.module.css';

/**
 * @typedef   {Object} Props
 * @property  {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param     {Props}
 */

function Navbar({ setPathname }) {
  const [show, setShow] = React.useState(false);

  /**
   * @param   {React.MouseEvent<HTMLAnchorElement>} event
   */

  const handleAnchorClick = (event) => {
    event.preventDefault();

    /**
     * @type  {HTMLAnchorElement}
     */

    const anchor = event.target;

    setPathname(anchor.pathname);
    setShow((state) => !state);
  };

  const handleShow = () => setShow((state) => !state);

  return (
    <nav className={Styles.container}>
      <h3 className={Styles.title}>React Stack</h3>

      <ul className={`${Styles['item-list']} ${show && `${Styles.show}`}`}>
        <li className={Styles['list-item']}>
          <a
            className={Styles['list-link']}
            href="/"
            onClick={handleAnchorClick}
          >
            Home
          </a>
        </li>
        <li className={Styles['list-item']}>
          <a
            className={Styles['list-link']}
            href="/game-list"
            onClick={handleAnchorClick}
          >
            Game List
          </a>
        </li>
      </ul>

      <Menu className={Styles.icon} onClick={handleShow} />
    </nav>
  );
}

Navbar.propTypes = { setPathname: PropTypes.func.isRequired };

export default React.memo(Navbar);
