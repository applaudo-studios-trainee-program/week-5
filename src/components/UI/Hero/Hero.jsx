import PropTypes from 'prop-types';

import Styles from './Hero.module.css';

/**
 * @typedef   {Object} Props
 * @property  {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param     {Props}
 */

function Hero({ setPathname }) {
  /**
   * @param   {React.MouseEvent<HTMLAnchorElement>} event
   */

  const handleAnchorClick = (event) => {
    event.preventDefault();

    /**
     * @type  {HTMLAnchorElement}
     */

    const anchor = event.target;

    setPathname(anchor.pathname);
  };

  return (
    <section className={Styles.container}>
      <h1 className={Styles.title}>React Stack</h1>
      <p className={Styles['sub-title']}>Your realiable game source</p>

      <a className={Styles.link} href="/game-list" onClick={handleAnchorClick}>
        Full Game List
      </a>
    </section>
  );
}

Hero.propTypes = {
  setPathname: PropTypes.func.isRequired,
};

export default Hero;
