import PropTypes from 'prop-types';

import Styles from './MetaInfo.module.css';

/**
 * @typedef     {Object} Props
 * @property    {string} title
 * @property    {string | []} content
 */

/**
 * @param       {Props}
 */

function MetaInfo({ title, content }) {
  return (
    <div className={Styles.container}>
      <h4 className={Styles.title}>{title}</h4>

      <p className={Styles.content}>
        {typeof content !== 'object'
          ? content
          : content.map((data, index) => {
              if (index === 0) return `${data.name}`;

              return `, ${data.name}`;
            })}
      </p>
    </div>
  );
}

MetaInfo.propTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.array]).isRequired,
};

export default MetaInfo;
