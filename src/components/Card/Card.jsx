import PropTypes from 'prop-types';

import Styles from './Card.module.css';

/**
 * @typedef {Object} GameArt
 * @property {string} url
 */

/**
 * @typedef   {Object} Props
 * @property  {string} name
 * @property  {GameArt} gameArt
 * @property  {React.Dispatch<React.SetStateAction<string>>} setPathname
 */

/**
 * @param     {Props}
 */

function Card({ name, price, publishers, gameArt }) {
  const { url } = gameArt;

  return (
    <div className={Styles.container}>
      <img
        className={Styles.image}
        src={url}
        alt={`${name} Cover`}
        title={`${name} Cover`}
      />

      <section className={Styles.content}>
        <p className={Styles.name}>{name}</p>

        {publishers.map((publisher) => (
          <p key={publisher.id} className={Styles['publisher-name']}>
            {publisher.name}
          </p>
        ))}

        <p className={Styles.price}>${price} USD</p>
      </section>
    </div>
  );
}

Card.propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  publishers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      created_at: PropTypes.string.isRequired,
      updated_at: PropTypes.string.isRequired,
    })
  ).isRequired,
  gameArt: PropTypes.shape({
    url: PropTypes.string.isRequired,
  }).isRequired,
};

export default Card;
