import PropTypes from 'prop-types';

import User from 'assets/Icons/User';
import Styles from './Comment.module.css';

/**
 * @typedef   {Object} Props
 * @property  {string} author
 * @property  {string} content
 */

/**
 * @param     {Props}
 */

function Comment({ author, content }) {
  return (
    <div className={Styles.container}>
      <div className={Styles.data}>
        <User className={Styles['data-icon']} />
        <p className={Styles['data-author']}>{author}</p>
      </div>

      <p className={Styles.content}>{content}</p>
    </div>
  );
}

Comment.propTypes = {
  author: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};

export default Comment;
