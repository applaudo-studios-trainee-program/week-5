import PropTypes from 'prop-types';

import Styles from './Button.module.css';

/**
 * @typedef   {Object} Props
 * @property  {string} content
 * @property  {boolean} [disabled]
 * @property  {() => void} onClickFn
 */

/**
 * @param     {Props}
 */

function Button({ content, disabled = false, onClickFn }) {
  return (
    <button
      className={Styles.button}
      disabled={disabled}
      onClick={onClickFn}
      type="button"
    >
      {content}
    </button>
  );
}

Button.propTypes = {
  content: PropTypes.string.isRequired,
  onClickFn: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default Button;
