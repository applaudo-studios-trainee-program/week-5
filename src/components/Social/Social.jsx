import PropTypes from 'prop-types';

import Styles from './Social.module.css';

/**
 * @typedef   {Object} Props
 * @property  {JSX.Element} socialIcon
 * @property  {string} username
 */

/**
 * @param     {Props}
 */

function Social({ socialIcon: SocialIcon, username }) {
  return (
    <div className={Styles.container}>
      <SocialIcon className={Styles.icon} />
      <p className={Styles.username}>{username}</p>
    </div>
  );
}

Social.propTypes = {
  socialIcon: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

export default Social;
