import { useState, useEffect, useRef, memo } from 'react';

import Navbar from 'components/UI/Navbar/Navbar';
import Footer from 'components/UI/Footer/Footer';
import Home from 'views/Home/Home';
import GameList from 'views/GameList/GameList';
import GameDetails from 'views/GameDetails/GameDetails';
import NotFound from 'views/NotFound/NotFound';
import { getParams, pathToRegExp } from 'helpers/routerHelpers';

/**
 * @typedef   {Object} Route
 * @property  {string} path
 * @property  {string} viewName
 */

/**
 * @typedef   {Object.<string, string>} Params
 */

/**
 * @type      {Route[]}
 */

const ROUTE_LIST = [
  { path: '/', viewName: 'Home' },
  { path: '/game-list', viewName: 'GameList' },
  { path: '/game-details/:id', viewName: 'GameDetails' },
];

const Router = () => {
  const historyAPIRef = useRef(window.history);

  const [params, setParams] = useState({});
  const [viewName, setViewName] = useState('Home');
  const [pathname, setPathname] = useState(window.location.pathname);

  const updateOnPopState = () => setPathname(window.location.pathname);

  useEffect(() => {
    const matchedRoute = ROUTE_LIST.find((route) =>
      pathToRegExp({ path: route.path }).test(pathname)
    ) || {
      path: '/not-found',
      viewName: 'NotFound',
    };

    const viewParams = getParams({ matchedRoute, pathname });

    setViewName(matchedRoute.viewName);
    setParams(viewParams);

    historyAPIRef.current.pushState(null, '', pathname);
  }, [pathname]);

  useEffect(() => {
    window.addEventListener('popstate', updateOnPopState);

    return () => window.removeEventListener('popstate', updateOnPopState);
  }, []);

  return (
    <>
      <header
        style={{
          backgroundColor: '#262828',
          position: 'sticky',
          top: 0,
          zIndex: 999,
        }}
      >
        <Navbar setPathname={setPathname} />
      </header>

      <main style={{ backgroundColor: '#0e1111' }}>
        {viewName === 'Home' ? (
          <Home setPathname={setPathname} />
        ) : viewName === 'GameList' ? (
          <GameList setPathname={setPathname} />
        ) : viewName === 'GameDetails' ? (
          <GameDetails params={params} />
        ) : (
          <NotFound />
        )}
      </main>

      <Footer setPathname={setPathname} />
    </>
  );
};

export default memo(Router);
