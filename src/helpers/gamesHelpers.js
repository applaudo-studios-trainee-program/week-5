const BASE_URL = 'https://trainee-gamerbox.herokuapp.com/';
const BASE_ENDPOINT = 'games';

/**
 * @returns {Promise.<number>}
 */

export const getGamesCount = async () => {
  const url = `${BASE_URL}${BASE_ENDPOINT}/count`;
  const response = await fetch(url);

  return response.json();
};

/**
 * @typedef {Object} GetGamesProps
 * @property {number} offset
 * @property {number} [actualPage]
 * @property {string} [sortBy]
 */

/**
 * @typedef {Object} GameArt
 * @property {string} url
 */

/**
 * @typedef {Object} Publisher
 * @property {number} id
 * @property {string} name
 */

/**
 * @typedef {Object} Platform
 * @property {number} id
 * @property {string} name
 */

/**
 * @typedef {Object} MetaInfo
 * @property {number} id
 * @property {string} title
 * @property {[] | string} content
 */

/**
 * @typedef {Object} GameData
 * @property {number} id
 * @property {string} name
 * @property {number} price
 * @property {GameArt} gameArt
 * @property {Publisher[]} publishers
 * @property {Platform[]} [platforms]
 * @property {MetaInfo[]} [metaInfo]
 */

/**
 * @param {GetGamesProps}
 * @returns {Promise<GameData[]>}
 */

export const getGames = async ({ offset, actualPage = 0, sortBy = 'name' }) => {
  const url = `${BASE_URL}${BASE_ENDPOINT}?_limit=${offset}&_start=${
    offset * actualPage
  }&_sort=${sortBy}`;

  const response = await fetch(url);
  const body = await response.json();

  /**
   * @type {GameData[]}
   */

  const gamesData = body.map((game) => {
    const { id, name, price, cover_art: coverArt, publishers } = game;

    /**
     * @type {GameArt}
     */

    const gameArt = {
      url:
        coverArt?.url ||
        'https://oij.org/wp-content/uploads/2016/05/placeholder.png',
    };

    /**
     * @type {GameData}
     */

    const gameData = {
      id,
      name,
      price,
      gameArt,
      publishers,
    };

    return gameData;
  });

  return gamesData;
};

/**
 * @typedef {Object} GetGameByIdProps
 * @property {number | string} gameId
 */

/**
 * @param {GetGameByIdProps}
 * @return {Promise.<GameData>}
 */

export const getGameById = async ({ gameId }) => {
  const url = `${BASE_URL}${BASE_ENDPOINT}/${gameId}`;

  const response = await fetch(url);

  const body = await response.json();

  const {
    name,
    price,
    release_year: releaseYear,
    genre: { name: genreName },
    comments,
    platforms,
    publishers,
  } = body;

  /**
   * @type {GameArt}
   */

  const gameArt = {
    url:
      body?.cover_art?.url ||
      'https://oij.org/wp-content/uploads/2016/05/placeholder.png',
  };

  /**
   * @type {MetaInfo[]}
   */

  const metaInfo = [
    { id: 0, title: 'Price', content: `$ ${price} USD` },
    { id: 1, title: 'Release Year', content: `${releaseYear}` },
    { id: 2, title: 'Genre', content: genreName.toUpperCase() },
    { id: 3, title: 'Publishers', content: publishers },
    { id: 4, title: 'Platforms', content: platforms },
  ];

  return { name, gameArt, metaInfo, comments };
};
