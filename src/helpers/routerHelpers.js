import matchAll from 'string.prototype.matchall';
import fromEntries from 'object.fromentries';

/**
 * @typedef   {Object.<string, string>} Params
 */

/**
 * @typedef   {Object} Route
 * @property  {string} path
 * @property  {string} viewName
 */

/**
 * @typedef   {Object} pathToRegExpProps
 * @property  {string} path
 */

/**
 * @param     {pathToRegExpProps}
 */

export const pathToRegExp = ({ path }) =>
  new RegExp(`^${path.replace(/\//g, '\\/').replace(/:\w+/g, '(.+)')}$`);

/**
 * @typedef   {Object} getParamsProps
 * @property  {string} pathname
 * @property  {Route} matchedRoute
 */

/**
 * @param     {getParamsProps}
 * @returns   {Params}
 */

export const getParams = ({ matchedRoute, pathname }) => {
  /**
   * @type    {string[] | []}
   */

  const paramList =
    pathname
      .match(pathToRegExp({ path: matchedRoute.path }))
      ?.filter((_, index) => index > 0) || [];

  /**
   * @type    {string[] | []}
   */

  const keys = Array.from(matchAll(matchedRoute.path, /:(\w+)/g)).map(
    (urlParam) => urlParam[1]
  );

  return fromEntries(
    Object.entries(keys).map((param, index) => [param[1], paramList[index]])
  );
};
