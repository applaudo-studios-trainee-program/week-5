import { useEffect, useState } from 'react';
import { getGames, getGamesCount } from 'helpers/gamesHelpers';

/**
 * @typedef {Object} Props
 * @property {number} itemsPerPage
 */

/**
 * @typedef {import('helpers/gamesHelpers').GameData} GameData
 */

/**
 * @param {Props}
 */

export function usePagination({ itemsPerPage }) {
  /**
   * @type {[GameData[], React.Dispatch<React.SetStateAction<GameData[]>>]}
   */

  const [itemsToShow, setItemsToShow] = useState([]);

  const [actualPage, setActualPage] = useState(0);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    getGamesCount().then((response) =>
      setTotalPages(Math.ceil(response / itemsPerPage) - 1)
    );
  }, []);

  useEffect(() => {
    getGames({ offset: itemsPerPage, actualPage }).then(setItemsToShow);
  }, [actualPage]);

  return { itemsToShow, actualPage, totalPages, setActualPage };
}
