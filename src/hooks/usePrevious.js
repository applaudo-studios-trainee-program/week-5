import { useEffect, useRef } from 'react';

/**
 * @param {{ value: string | number | boolean }}
 */

export const usePrevious = ({ value }) => {
  const ref = useRef(null);

  useEffect(() => {
    ref.current = value;
  });

  return ref.current;
};
